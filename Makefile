.RECIPEPREFIX+= 

all: main

run: main
  ./main

main: src/main.cpp
  clang++ src/main.cpp -lGL -lglut -lSOIL -std=c++11 -o main -Wall #-ferror-limit=3

src/%.cpp:
  true

.PHONY: all clean run

clean:
  rm main
