#include <stdio.h>
#include <limits>
#include <vector>

using namespace std;

template <typename T>
struct factor {
    T value;
    factor(T x) {
        value = x;
    }
    factor(double x) {
        value = (T)(x*std::numeric_limits<T>::max());
    }
    factor operator=(double x) {
        value = (T)(x*std::numeric_limits<T>::max());
        return *this;
    }
    double to_double() {
        return value/(double)std::numeric_limits<T>::max();
    }
    float to_float() {
        return value/(float)std::numeric_limits<T>::max();
    }
    factor inverse() {
        return factor(~value);
    }
    factor shrink(T n) {
        return factor((value >> n)+((value&(1<<(n-1)))!=0?(unsigned int)1:(unsigned int)0));
    }
    factor grow(T n) {
        return inverse().shrink(n).inverse();
    }
    factor operator+=(factor x) {
        value += x.value;
        return *this;
    }
    factor operator-=(factor x) {
        value -= x.value;
        return *this;
    }
};

struct Edge {
    unsigned int from, to, fac;
};

struct Evolution {
    vector<factor<unsigned int>> state;
    vector<Edge> edges;
    void act() {
        for(auto& edge: edges) {
            auto inc = state[edge.from].shrink(edge.fac);
            state[edge.to] += inc;
            state[edge.from] += inc;
        }
    }
    void print() {
        for(auto& value: state) {
            printf("%f; ", value.to_float());
        }
        printf("\n");
    }
};

int main() {
    factor<unsigned int> x = 4u;
    auto y = x.shrink(0);
    printf("%u\n%u\n", x, y);
}

